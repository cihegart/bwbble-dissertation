#!/bin/bash

# assumes you have to aws cli installed, and configured correctly, as well as
# having access to the key file used in your created key pair

# using image for Ubuntu 18.04 LTS
# find security group from the aws management console for ec2

# takes in the number of instances to start as a command line argument
# 2nd argument - if t then instances are terminated after exiting from master instance
# 3rd argument - if t deletes all mounts and EFS

# these variables based on the settings you have created via the aws UI
region="us-east-2"
securityGroup="sg-0733489fbc1ac3b93"
slaveKeyPairName="slave-keypair"
slaveKeyPairFileName="~/Downloads/slave-keypair.pem"
masterKeyPairName="aws-keypair"
masterKeyPairPath="~/Downloads/aws-keypair.pem"
IAMProfileName="remote-user"
instanceType="t2.large"
subnet1ID="subnet-a5390ecd" # us-east-2a
subnet2ID="subnet-43563b39" # us-east-2b
subnet3ID="subnet-f59743b9" # us-east-2c
numInstances=${1:-1}





echo "Creating EFS..."
EFS_description=`aws efs create-file-system --creation-token FileSystemForBWBBLE \
                                            --region $region`

#echo "$EFS_description"

EFS_ID=` echo $EFS_description | jq -r '.FileSystemId'`
echo "EFS_ID: $EFS_ID"
#sleep to give the efs time to get spun up
sleep 10s
# a mount is needed to allow the instaces to use the efs
mount1Desc=`aws efs create-mount-target --file-system-id $EFS_ID \
          --subnet-id $subnet1ID \
          --security-group $securityGroup \
          --region $region`
mount1ID=` echo $mount1Desc | jq -r '.MountTargetId'`
echo "Mount Target 1 ID: "$mount1ID""

mount2Desc=`aws efs create-mount-target --file-system-id $EFS_ID \
          --subnet-id $subnet2ID \
          --security-group $securityGroup \
          --region $region`
mount2ID=` echo $mount2Desc | jq -r '.MountTargetId'`
echo "Mount Target 2 ID: "$mount2ID""

mount3Desc=`aws efs create-mount-target --file-system-id $EFS_ID \
          --subnet-id $subnet3ID \
          --security-group $securityGroup \
          --region $region`
mount3ID=` echo $mount3Desc | jq -r '.MountTargetId'`
echo "Mount Target 3 ID: "$mount3ID""



echo "Number of Instances: "$numInstances""


#creates slaves
slaves=`aws ec2 run-instances --image-id ami-0f65671a86f061fcd \
                              --security-group-ids $securityGroup \
                              --iam-instance-profile Name=$IAMProfileName \
                              --count $numInstances \
                              --region $region \
                              --instance-type $instanceType \
                              --key-name $slaveKeyPairName \
                              --user-data file://init-slave.sh`

#echo $slaves
slaveIDs=` echo $slaves | jq -r '.Instances[].InstanceId'`
echo "Slave Instances IDs: $slaveIDs"
#need the IDs of the slaves so we can shut them all down
readarray -t slavesIDArray <<<"$slaveIDs"

#this gets the IP of the slave instance
slaveIP=`aws ec2 describe-instances --instance-ids $slavesIDArray \
                        --query "Reservations[0].Instances[0].PublicIpAddress"`

slaveIP="${slaveIP%\"}"
slaveIP="${slaveIP#\"}"
#echo "Slave Instance IP: "$slaveIP""

#remove file of slave IPs from previous run
#get ip addresses of slaves and write to a file which should be uploaded to
#master instance
#scp file to the master instance so that when bwbble alignment is run it can
#ssh to the other instances and tell them what sections of the reads to align

slaveIPs=`aws ec2 describe-instances --query "Reservations[].Instances[].PublicIpAddress"`
readarray -t slavesIPArray <<<"$slaveIPs"
rm slaveIPs.txt
for index in ${!slavesIPArray[@]}; do
  if [[ "${slavesIPArray[$index]}" != "${slavesIPArray[0]}" && "${slavesIPArray[$index]}" != "${slavesIPArray[-1]}" ]]; then
    instanceIP=${slavesIPArray[$index]}
    instanceIP="${instanceIP//\"}"
    instanceIP="${instanceIP%\,}"
    #echo $instanceIP
    echo $instanceIP >> slaveIPs.txt
  fi
done
#remove ,'s that are sometimes left behind
sed -i 's/,$//' slaveIPs.txt

# creates master instance
# security group should probably only allow connections from my IP
# curl ifconfig.me gives you your own ip address

instanceRun=`aws ec2 run-instances --image-id ami-0f65671a86f061fcd \
                             --security-group-ids $securityGroup \
                             --iam-instance-profile Name=$IAMProfileName \
                             --count 1 \
                             --region $region \
                             --instance-type $instanceType \
                             --key-name $masterKeyPairName \
                             --user-data file://init-master.sh`


#nice for debug purposes to see what you've created
#echo "$instanceRun"

instanceID=` echo $instanceRun | jq -r '.Instances[0].InstanceId'`

# removing quotation marks from around instanceID
instanceID="${instanceID%\"}"
instanceID="${instanceID#\"}"
echo "Master Instance ID: "$instanceID""

#for debug purposes
#aws ec2 describe-instances --instance-ids $instanceID


#this gets the IP of the master instance
instanceIP=`aws ec2 describe-instances --instance-ids $instanceID \
                        --query "Reservations[0].Instances[0].PublicIpAddress"`
instanceIP="${instanceIP%\"}"
instanceIP="${instanceIP#\"}"
echo "Master Instance IP: "$instanceIP""

instanceState="booting"
instanceSysStatus=""
instanceStatus=""
readyState="running"
initialised="ok"
# might need to revisit to get optimum time for sleeping
sleepTime=144
echo "Waiting for instance to boot..."
# this approach is a lot faster, only takes about a minute but instance usually isn't ready anyway
# scp -i ~/Downloads/aws-keypair.pem -o "StrictHostKeyChecking no" slaveIPs.txt ubuntu@$instanceIP:/home/ubuntu/.
# while [$? != 0]
# do
#   scp -i ~/Downloads/aws-keypair.pem -o "StrictHostKeyChecking no" slaveIPs.txt ubuntu@$instanceIP:/home/ubuntu/.
# done
# dont connect until state is running, and the initialisation checks are completed
#sleep "$sleepTime"s
while [[ "$instanceState" != "$readyState" || "$instanceSysStatus" != "$initialised" || "$instanceStatus" != "$initialised" ]];
do
  instanceDescription=`aws ec2 describe-instance-status  --include-all-instances  \
                                                  --instance-id $instanceID`
  #echo $instanceDescription
  instanceState=` echo $instanceDescription | jq -r '.InstanceStatuses[0].InstanceState.Name'`
  #echo "Instance State: "$instanceState""

  instanceSysStatus=` echo $instanceDescription | jq -r '.InstanceStatuses[0].SystemStatus.Status'`
  #echo "Instance System Status: "$instanceSysStatus""

  instanceStatus=` echo $instanceDescription | jq -r '.InstanceStatuses[0].InstanceStatus.Status'`
  #echo "Instance Status: "$instanceStatus""
  #sleepTime=$((sleepTime/4))
  #echo "Sleep time: $sleepTime"
  #echo "........"
  #sleep "$sleepTime"s
done
echo "-----------------------------------------------------"
echo "Instance State is "$instanceState""
echo "Instance Status is "$instanceStatus""
echo "Instance System Status is "$instanceSysStatus""

if (( $SECONDS > 3600 )) ; then
    let "hours=SECONDS/3600"
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $hours hour(s), $minutes minute(s) and $seconds second(s)"
elif (( $SECONDS > 60 )) ; then
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $minutes minute(s) and $seconds second(s)"
else
    echo "Completed in $SECONDS seconds"
fi
echo "Uploading slave IP addresses file to master instance..."
scp -i $masterKeyPairPath -o "StrictHostKeyChecking no" slaveIPs.txt ubuntu@$instanceIP:/home/ubuntu/.
scp -i $masterKeyPairPath -o "StrictHostKeyChecking no" $slaveKeyPairFileName ubuntu@$instanceIP:/home/ubuntu/.

echo "Connecting to master instance..."
# connects to the instance and skips checks
ssh -i $masterKeyPairPath -o "StrictHostKeyChecking no" ubuntu@$instanceIP

#if the second argument is "t" then the instance will be terminated after
#disconnecting from the instance
if [[ $2 = "t" ]]; then
  aws ec2 terminate-instances --instance-ids $instanceID
  for index in ${!slavesIDArray[@]}; do
      echo ${slavesIDArray[$index]}
      aws ec2 terminate-instances --instance-ids ${slavesIDArray[$index]}
  done
  echo "Terminated instance(s)"
fi

#if the third argument is "t" then the efs will be deleted after exiting the console
# mount targets must be deleted first
if [[ $3 = "t" ]]; then
  aws efs delete-mount-target --mount-target-id $mount1ID
  aws efs delete-mount-target --mount-target-id $mount2ID
  aws efs delete-mount-target --mount-target-id $mount3ID
  echo "Deleted Mount Targets"
  sleep 10s
  aws efs delete-file-system --file-system-id $EFS_ID
  echo "Deleted File System"
fi
