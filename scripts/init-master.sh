#!/bin/bash
# script to initialise BWBBLE on the master instance
# can't run with user feedback, must automaticlly run
sudo apt update
region="us-east-2"
subnetID="subnet-a5390ecd"

sudo apt install make gcc zlib1g-dev nfs-common jq python -y
cd /home/ubuntu/
curl -O https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install awscli
aws --version

EFS_description=`aws efs describe-file-systems --region $region`
echo "$EFS_description"
EFS_ID=` echo $EFS_description | jq -r '.FileSystems[0].FileSystemId'`
echo "EFS_ID: $EFS_ID"
mountDesc=`aws efs describe-mount-targets --file-system-id $EFS_ID \
          --region $region`
echo "$mountDesc"
mountIP=`echo $mountDesc | jq -r '.MountTargets[0].IpAddress'`
mountID=`echo $mountDesc | jq -r '.MountTargets[0].MountTargetId'`
echo "mountIP: $mountIP"

cd /home/ubuntu/
sudo mkdir efs
sudo mount -t nfs4 \
           -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport \
           --source "$mountIP:/" \
           --target efs

sudo chmod -R 777 efs/
cd efs
#just takes a bit longer to clone the full repo
git clone http://gitlab.scss.tcd.ie/cihegart/bwbble-dissertation.git /home/ubuntu/efs/bwbble-dissertation
sudo chmod -R 777 /home/ubuntu/efs/
cd /home/ubuntu/efs/bwbble-dissertation/bwbble/mg-aligner
sudo make clean
sudo make

cd /home/ubuntu/efs/bwbble-dissertation/scripts
./mul_contents.sh ../bwbble/test_data/sim_chr1_N1000000.fastq ../bwbble/test_data/12mill.fastq

# change permissions on slave keypair
cd /home/ubuntu/
sudo chmod 400 slave-keypair.pem


echo "Completed in $SECONDS seconds"
