#!/bin/bash
# script to initialise BWBBLE on each slave instance
# can't run with user feedback, must automaticlly run
sudo apt update
region="us-east-2"
subnetID="subnet-a5390ecd"

sudo apt install make gcc zlib1g-dev nfs-common jq python -y
cd /home/ubuntu/
curl -O https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install awscli
aws --version

EFS_description=`aws efs describe-file-systems --region $region`
echo "$EFS_description"
EFS_ID=` echo $EFS_description | jq -r '.FileSystems[0].FileSystemId'`
echo "EFS_ID: $EFS_ID"
mountDesc=`aws efs describe-mount-targets --file-system-id $EFS_ID \
          --region $region`
echo "$mountDesc"
mountIP=`echo $mountDesc | jq -r '.MountTargets[0].IpAddress'`
mountID=`echo $mountDesc | jq -r '.MountTargets[0].MountTargetId'`

sudo mkdir /home/ubuntu/efs
sudo mount -t nfs4 \
           -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport \
           --source "$mountIP:/" \
           --target efs
cd /home/ubuntu/efs
sudo chmod go+rw .

echo "Completed in $SECONDS seconds"
