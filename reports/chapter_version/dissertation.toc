\babel@toc {english}{}
\contentsline {chapter}{List of Figures}{ix}{chapter*.8}
\contentsline {chapter}{List of Tables}{x}{chapter*.9}
\contentsline {chapter}{List of Listings}{xi}{chapter*.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Aim}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Motivations}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Paper Overview}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}State of the Art}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}DNA Alignment}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Read Alignment Programs}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Bwbble}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Read Alignment Programs based on Apache Spark}{7}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}SparkBwbble}{11}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Amazon Web Services (AWS)}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Elastic Compute Cloud (EC2)}{12}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Elastic File System (EFS)}{13}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Amazon Web Services Command Line Interface (AWS-CLI)}{14}{subsection.2.3.3}
\contentsline {chapter}{\numberline {3}Design}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Architecture}{16}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}AWS Components}{17}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}BwbbleC}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Usage}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}AWS Startup Script}{18}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}BwbbleC Program}{18}{subsection.3.3.2}
\contentsline {chapter}{\numberline {4}Implementation}{22}{chapter.4}
\contentsline {section}{\numberline {4.1}Creating Initial AWS Infrastructure}{22}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}start\_aws.sh}{22}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}init\_master.sh}{24}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}init\_slave.sh}{25}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}BwbbleC}{25}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Alignment code running on Master AWS Instance}{26}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Alignment code running on Slave AWS Instances}{27}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Verifying Results}{29}{section.4.3}
\contentsline {chapter}{\numberline {5}Results and Discussion}{30}{chapter.5}
\contentsline {section}{\numberline {5.1}Initial Setup Time}{30}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Master Instance Initialisation Script Execution Time}{31}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Slave Instance Initialisation Script Execution Time}{33}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Alignment Speedup}{34}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Comparing Speedup to SparkBwbble}{35}{subsection.5.2.1}
\contentsline {chapter}{\numberline {6}Conclusions}{37}{chapter.6}
\contentsline {chapter}{\numberline {7}Future Work}{39}{chapter.7}
\contentsline {section}{\numberline {7.1}Improve AWS Scripts}{39}{section.7.1}
\contentsline {section}{\numberline {7.2}Improve BwbbleC Code}{40}{section.7.2}
\contentsline {section}{\numberline {7.3}Parallelise Bwbble alignment using GPUs}{41}{section.7.3}
\contentsline {chapter}{Bibliography}{43}{chapter*.19}
