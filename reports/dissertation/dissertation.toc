\babel@toc {english}{}
\contentsline {section}{List of Figures}{ix}{section*.8}
\contentsline {section}{List of Tables}{x}{section*.10}
\contentsline {section}{List of Listings}{xi}{section*.12}
\contentsline {section}{\numberline {1}Introduction}{1}{section.0.1}
\contentsline {subsection}{\numberline {1.1}Aim}{2}{subsection.0.1.1}
\contentsline {subsection}{\numberline {1.2}Motivations}{2}{subsection.0.1.2}
\contentsline {subsection}{\numberline {1.3}Paper Overview}{3}{subsection.0.1.3}
\contentsline {section}{\numberline {2}State of the Art}{5}{section.0.2}
\contentsline {subsection}{\numberline {2.1}DNA Alignment}{5}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}Read Alignment Programs}{6}{subsection.0.2.2}
\contentsline {subsubsection}{Bwbble}{6}{section*.15}
\contentsline {subsubsection}{Read Alignment Programs based on Apache Spark}{7}{section*.16}
\contentsline {subsubsection}{SparkBwbble}{11}{section*.17}
\contentsline {subsection}{\numberline {2.3}Amazon Web Services (AWS)}{11}{subsection.0.2.3}
\contentsline {subsubsection}{Elastic Compute Cloud (EC2)}{12}{section*.18}
\contentsline {subsubsection}{Elastic File System (EFS)}{13}{section*.19}
\contentsline {subsubsection}{Amazon Web Services Command Line Interface (AWS-CLI)}{13}{section*.20}
\contentsline {section}{\numberline {3}Design}{15}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Architecture}{16}{subsection.0.3.1}
\contentsline {subsubsection}{AWS Components}{16}{section*.22}
\contentsline {subsection}{\numberline {3.2}BwbbleC}{17}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}Usage}{18}{subsection.0.3.3}
\contentsline {subsubsection}{AWS Start Up Script}{18}{section*.23}
\contentsline {subsubsection}{BwbbleC Program}{18}{section*.24}
\contentsline {section}{\numberline {4}Implementation}{21}{section.0.4}
\contentsline {subsection}{\numberline {4.1}Creating Initial AWS Infrastructure}{21}{subsection.0.4.1}
\contentsline {subsubsection}{start\_aws.sh}{21}{section*.25}
\contentsline {subsubsection}{init\_master.sh}{23}{section*.26}
\contentsline {subsubsection}{init\_slave.sh}{23}{section*.27}
\contentsline {subsection}{\numberline {4.2}BwbbleC}{24}{subsection.0.4.2}
\contentsline {subsubsection}{Alignment code running on Master AWS Instance}{25}{section*.28}
\contentsline {subsubsection}{Alignment code running on Slave AWS Instances}{26}{section*.30}
\contentsline {subsection}{\numberline {4.3}Verifying Results}{28}{subsection.0.4.3}
\contentsline {section}{\numberline {5}Results and Discussion}{29}{section.0.5}
\contentsline {subsection}{\numberline {5.1}Initial Setup Time}{29}{subsection.0.5.1}
\contentsline {subsubsection}{Master Instance Initialisation Script Execution Time}{30}{section*.32}
\contentsline {subsubsection}{Slave Instance Initialisation Script Execution Time}{32}{section*.34}
\contentsline {subsection}{\numberline {5.2}Alignment Speed-up}{32}{subsection.0.5.2}
\contentsline {subsubsection}{Comparing Speedup to SparkBwbble}{33}{section*.37}
\contentsline {section}{\numberline {6}Conclusions}{36}{section.0.6}
\contentsline {section}{\numberline {7}Future Work}{38}{section.0.7}
\contentsline {subsection}{\numberline {7.1}Improve AWS Scripts}{38}{subsection.0.7.1}
\contentsline {subsection}{\numberline {7.2}Improve BwbbleC Code}{39}{subsection.0.7.2}
\contentsline {subsection}{\numberline {7.3}Parallelise Bwbble alignment using GPUs}{40}{subsection.0.7.3}
