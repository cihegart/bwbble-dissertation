
\include{preamble}
\begin{document}
\pagenumbering{arabic}
	\begin{center}
    	\Huge Ciaran Hegarty

   		\Large 14316285

    	cihegart@tcd.ie

    	Supervisor: Prof. Jeremy Jones

    	\vspace{20pt}

		\Huge Research Proposal: Cloud Based Adaptation of DNA Alignment
    \end{center}

    \include{declaration}
	\tableofcontents
    %\include{introduction}
    \section{Introduction}

In next-generation sequencing, DNA alignment is the process of mapping reads to a reference genome, where a read is a string of nucleotides \hyperlink{dna}{[1]} in a specific order, and a genome is the complete set of genetic material in an organsim.

To aid in DNA alignments there have been multiple short-read alignment programs developed; BWA \hyperlink{bwa}{[2]}, SOAP2 \hyperlink{soap2}{[3]}, and BOWTIE \hyperlink{bowtie}{[4]}. These alignment programs all rely on the compression algorithm, the Burrows-Wheeler-Transform (BWT) \hyperlink{bwt}{[5]}. These programs can only map reads to a single genome, whereas BWBBLE uses the concept of a multi-genome that incorporates the different genomic variants, and the accompanying alignment algorithm to accurately map reads to this multi-genome \hyperlink{bwbble}{[6]}.

The aim of this research is to speed up the analysis of the BWBBLE program \hyperlink{bwbble}{[6]} by running it in parallel on AWS, Amazon's cloud platform. There is also the potential to improve the BWBBLE source code to increase its performance when running locally.

There are two main questions that will be answered through this research:
\begin{enumerate}
	\item Is it possible to speed up the alignment process in the BWBBLE program by running it in parallel across a cloud service?
	\item Are there any improvements to be made to the BWBBLE program to reduce its runtimes?
\end{enumerate}

\section{Motivations}

BWBBLE has the ability to map reads to a multi-reference genome but comes at the cost of the runtime \hyperlink{bwbble}{[6]}. The following table shows how both BWA and BWBBLE perform with different simulated read data in an experiment carried out by Lin Huang et al. BWA aligned reads against a single genome whereas BWBBLE aligned reads against a multi-genome \hyperlink{bwbble}{[6]}.

\begin{table}[h!]
\centering
\begin{tabular}{l c c c}
 \hline
 \textbf{Program} & \textbf{Confidence(\%)} & \textbf{Error(\%)} & \textbf{Time(s)} \\ [0.5ex]
 \hline\hline
 bwa-simR &	93.1 &	0.06 &	100 \\
bwbble-simR &	92.9 &	0.05 &	11295 \\
bwa-simNA &	92.7 &	0.09 &	110 \\
bwbble-simNA &	93.4 &	0.04 &	10896 \\
bwa-simHG &	92.6 &	0.11 &	107 \\
 bwbble-simHG & 93.3 & 0.06 & 10892  \\ [1ex]
 \hline
 \end{tabular}
\end{table}

If the BWBBLE alignment was faster then it would be more popular because it can align against multi-reference genomes and is still highly accurate. The idea behind adapting BWBBLE to run on a cloud computing platform is to scale resources up and down depending on the data size and the cost of the cloud platform, and AWS lends itself perfectly to this task \hyperlink{bwbble}{[7]}.
    %\include{motiv}
    %\include{abbrevs}
    \section{Table of Abbreviations}
\begin{table}[h!]
\centering
\begin{tabular}{||c | c||}
 \hline
 Abbreviation & Full Form \\ [0.5ex]
 \hline\hline
 DNA & Deoxyribonucleic acid \\
 NGS & Next-Generation Sequencing \\
 BWT & Burrows-Wheeler-Transform \\
 BWA & Burrows-Wheeler Aligner \\
 AWS & Amazon Web Services \\
 PMC & PubMed Central \\
 NCBI & National Centre for Biotechnology Information \\
 VM & Virtual Machine \\
 SW & Smith-Waterman \\ [1ex]
 \hline
 \end{tabular}
\end{table}
   % \include{prevwork}
    \section{Prior Work}

A previous attempt at porting BWBBLE to a cloud platform was carried out as a final year project in 2018 by Ben Stratford. This project introduced SparkBwbble, which is an adaption of the cluster alignment tool SparkBWA which was built to parallelise BWA \hyperlink{sparkbwbble}{[8]}.

SparkBwbble succeeded in making BWBBLE a more viable alignment program by improving the runtime but the use of the Spark framework added an overhead which limited potential further improvements.

It's speculated that a simpler approach to running BWBBLE on a cloud platform will lead to significiantly improved runtimes, which is what will be attempted in this piece of research. Rather than using a framework to support error recovery, this research aims to be efficient enough to simply run again should any unlikely errors occur, such as hardware failures.
    %\include{litsourcelist}
    \section{Literature}

\subsection{Keywords}
\begin{itemize}
	\item DNA
	\item Parallel
	\item Cloud
	\item Reads
	\item Alignment
	\item BWT
	\item NGS
	\item Genome
	\item Read Alignment
\end{itemize}
\subsection{Literature Source List}
\begin{itemize}
	\item Web of Science
	\item National Centre for Biotechnology Information (NCBI)
	\item PubMed Central (PMC)
	\item Google
\end{itemize}

\subsection{Literature Source Strategy}

The strategy adopted in search for relevant literature was to search PMC as that contained the paper on BWBBLE and would most likely have similar papers. If this database failed to yield any relevant results then the search was expanded to search the entire NCBI database, then finally Web of Science or Google.

Other techniques used were following the references of relevant papers. For example, Lin Huang's paper, Short read alignment with populations of genomes \hyperlink{bwbble}{[6]}, contained references to other alignment programs.

To find more recent work in this area, papers that had cited Lin Huang's paper \hyperlink{bwbble}{[6]} were searched for as they would most likely refer to the most recent work in this area. These two techniques created a timeline which could be traversed for both old and new work in the area.
    %\include{method}
    \section{Methodology}

Reading relevant papers in this area provides some context on what methodology has and has not worked for similar programs being ported to a cloud platform. The best method will be found by testing the feasability of each approach. Once the ideal approach is found, then the work will begin on applying it to the full scale BWBBLE program. Several ideas and approaches being considered so far include:
\begin{itemize}
	\item Run the BWT index on one VM in the cloud
	\item Automatically spawn VMs to align specific reads based on the number of reads
	\item Using OpenMPI for communication between the VMs
	\item A shared file system
	\item Combining the results from each VM
\end{itemize}

To answer the second half of the research question, "can the BWBBLE program runtime be improved locally?", different aspects of the BWBBLE source code must be examined:
\begin{itemize}
	\item Algorithms
	\item Data structures
	\item Temporal and spatial locality within the code
	\item How the code runs in parallel?
\end{itemize}

As work progresses, new areas for improvement may come to light.
    %\include{evaluation}
    \section{Evaluation}
To evaluate the program, a benchmark must be set as a baseline from which improvement can be measured. This can be done by creating a file with enough reads that causes BWBBLE to take a substantial amount of time align them. This allows for easy comparison of runtimes from before and after the research. A linear improvement of runtime is expected.

Several benchmarks will be created for varying amounts of reads and for varying multi-genomes to give complete and reliable proof that the speed ups gained apply to all the different types of data that BWBBLE supports. The runtime of BWBBLE executing on the cloud could be compared to other alignment programs running the same alignment locally.

    %\include{researcharea}
    \section{Research Area}

DNA aligment is essential in the re-sequencing process. Re-sequencing is sequencing more organisms of the same species in order to see the genetic differences compared with the original reference sequence. NGS produces short reads of generally 200 DNA base pairs but these reads needs to be aligned to a genome that is very long, typically several billion bases long and there could be millions of reads that need to be mapped to the genome \hyperlink{alignment}{[9]}.

NGS technologies are producing genomic data of longer sequences and as a result require faster programs to align the reads in a shorter amount of time. In recent years, NGS has seen a large increase in the amount of parallel implementations of different programs as shown in the table below from Runxin Guo, et al (2018) \hyperlink{sparkapp}{[10]}.

\begin{table}[h!]
\centering
\begin{tabular}{| m{2cm} m{2cm} m{3cm} m{3cm} m{2cm} |} 
 \hline
 \textbf{Name} & \textbf{Function} & \textbf{Features} & \textbf{Pros/Cons} & \textbf{Reference} \\
 \hline\hline
 SparkSW & Alignment
and mapping & Consists of three phases: data preprocessing, SW as map tasks, and top K records as reduce tasks & Load-balancing, scalable, but without the mapping location and traceback of optimal alignment & \hyperlink{sparksw}{[11]} \\
 \hline
DSA & Alignment and mapping & Leverages data parallel strategy based on SIMD instruction & Up to 201 times increased speed over SparkSW and almost linearly increased speed with increasing numbers of cluster nodes & \hyperlink{dsa}{[12]} \\
 \hline
 CloudSW & Alignment and mapping & Leverages SIMD instruction, and provides API services in the cloud & Up to 3.29 times increased speed over DSA and 621 times increased speed over SparkSW; high scalability and efficiency & \hyperlink{cloudsw}{[13]} \\ [1ex]
 \hline
 \end{tabular}
\end{table}
    %\include{ethics}
    \section{Ethics}

The development of the project does not require permission from the ethics committee as there will be no sensitive data used at any point. The actual contents of the data is insignificant, the data only has to be large enough to guarantee a sizeable benchmark.
    %\include{benefits}
    \section{Benefits of this Research}

The BWBBLE program is highly accurate so improving its runtime will give extra incentive to use it. If it's as fast as the other alignment programs, as accurate as other alignment programs, and supports multi-reference genomes then users will find this an excellent tool to use in their work in NGS.
    \include{toc}

    \include{skills}

    %\include{problems}
    \section{Anticipated Problems}

There aren't any anticipated problems at this point.
    \section{Gantt Chart}
\includegraphics[scale=0.5]{ganttchart.png}

\begin{table}[h!]
\centering
\captionof{table}{Critical Tasks and Phases} \label{tab:title}
\begin{tabular}{|| c | c | c ||}
 \hline
 Start Date & End Date & Task \\ [0.5ex]
 \hline\hline
17/12/18	& 28/1/19 &	State of the Art \\
7/1/2019	& 28/2/19	& BWBBLE parallelised on AWS \\
14/2/19	& 22/3/19	& Improve BWBBLE source code \\
7/1/19	& 12/4/19	& Writing Dissertation \\[1ex]
 \hline
 \end{tabular}
\end{table}


\begin{table}[h!]
\centering
\captionof{table}{Important Dates} \label{tab:title}
\begin{tabular}{|| c | c ||}
 \hline
 Due Date & Milestone \\ [0.5ex]
 \hline\hline
26/3/19 &	Demonstration Period Begins	 \\
5/4/19 &	Presentation and Poster Submission Date \\
9/4/19 &	Presentation and Poster Session	\\
12/4/19 &	Dissertation Due \\[1ex]
 \hline
 \end{tabular}
\end{table}

    %\include{gantt}
    %\include{dissemination}
    \section{Research Dissemination Plan}
As a first step towards spreading this research, a seminar could be given in Trinity aimed towards computer science and bioinformatics students. Applying to publish the research paper in the European Conference on Computational Biology would be a huge step in informing parties who would be interested in the research. It is significantly more difficult to publish a paper in the Bioinformatics journal but this could be possible if the paper is received well at a conference.
    %\include{references}
    \begin{thebibliography}{99}

\bibitem{dna}
\hypertarget{dna}
Athel Conrish-Bowden.
\textit{Nomenclature for incompletely specified bases in nucleic acid sequences: recommendations} (1984).
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC341218/

\bibitem{bwa}
\hypertarget{bwa}
Li H, Durbin R.
\textit{Fast and accurate short read alignment with burrows-wheeler transform} (2009).
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2705234/

\bibitem{soap2}
\hypertarget{soap2}
Li R, et al.
\textit{Soap2: an improved ultrafast tool for short read alignment} (2009).
https://academic.oup.com/bioinformatics/article/25/15/1966/212427

\bibitem{bowtie}
\hypertarget{bowtie}
Langmead B, et al.
\textit{Ultrafast and memory-efficient alignment of short DNA sequences to the human genome} (2009).
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2690996/

\bibitem{bwt}
\hypertarget{bwt}
Michael Burrows, David Wheeler.
\textit{A Block-sorting Lossless Data Compression Algorithm}
(1994).
http://www.hpl.hp.com/techreports/Compaq-DEC/SRC-RR-124.pdf

\bibitem{bwbble}
\hypertarget{bwbble}
Lin H, et al.
\textit{Short read alignment with populations of genomes} (2013).
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3694645/
https://github.com/viq854/bwbble

\bibitem{aws}
\hypertarget{aws}
AWS.
\textit{AWS Website} (2018).
https://aws.amazon.com/what-is-cloud-computing/?nc1=f\_cc

\bibitem{sparkbwbble}
\hypertarget{sparkbwbble}
Ben Stratford.
\textit{Cloud-based high speed parallel DNA read alignment
using the Burrows-Wheeler Transform} (2018).
https://scss.tcd.ie/Jeremy.Jones/bioinformatics/Ben\%20Stratford/Ben\%20Stratford\%20FYP\%202017-18.pdf

\bibitem{alignment}
\hypertarget{alignment}
En.Wikibooks.Org
\textit{Next Generation Sequencing (NGS)/Alignment} (2018).
https://en.wikibooks.org/wiki/Next\_Generation\_Sequencing\_(NGS)/Alignment.

\bibitem{sparkapp}
\hypertarget{sparkapp}
Runxin Guo, et al.
\textit{Bioinformatics applications on Apache Spark} (2018).
https://academic-oup-com.elib.tcd.ie/gigascience/article/7/8/giy098/5067872

\bibitem{sparksw}
\hypertarget{sparksw}
Zhao G, Ling C, Sun D.
\textit{SparkSW: scalable distributed computing system for large-Sscale Bbiological sequence alignment.}
In: Ieee/acm International Symposium on Cluster, Cloud and Grid Computing: 2015 . 845–52.

\bibitem{dsa}
\hypertarget{dsa}
Xu B, Li C, Zhuang H, et al.
\textit{DSA: scalable distributed sequence alignment system using SIMD instructions.}
In: Ieee/acm International Symposium on Cluster, Cloud and Grid Computing: 2017 . 758–61.

\bibitem{cloudsw}
\hypertarget{cloudsw}
Xu B, Li C, Zhuang H et al.
\textit{Efficient distributed Smith-Waterman algorithm based on Apache Spark.}
In: IEEE International Conference on Cloud Computing: 2017 . 608–15.

\end{thebibliography}


\end{document}
